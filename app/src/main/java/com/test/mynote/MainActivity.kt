package com.test.mynote

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.test.mynote.service.ConnectivityReceiver
import com.test.mynote.service.MyService


class MainActivity : AppCompatActivity() ,  ConnectivityReceiver.ConnectivityReceiverListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        registerReceiver(
            ConnectivityReceiver(),
            IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        )
        ConnectivityReceiver.connectivityReceiverListener = this
    }


    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        var messageToUser = ""
        if (isConnected) {

            messageToUser = "You are online now."

        } else {
            messageToUser = "You are offline now."
        }


        val builder = NotificationCompat.Builder(
            applicationContext!!,
            CHANNEL_ID
        )
            .setSmallIcon(R.drawable.unnamed)
            .setContentTitle("Network Change")
            .setContentText(messageToUser)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setAutoCancel(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, messageToUser, importance).apply {
                messageToUser
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
            notificationManager.notify(1001, builder.build())

        } else {
            val notificationManager = NotificationManagerCompat.from(applicationContext)
            notificationManager.notify(1001, builder.build())
        }
    }

    companion object {
        const val CHANNEL_ID = "ss"
    }
}