package com.test.mynote

import android.app.*
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.observe
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.test.mynote.database.Note
import com.test.mynote.viewmodel.NoteViewModel
import com.test.mynote.viewmodel.NoteViewModelFactory
import java.util.*


class NoteDetails : AppCompatActivity() {

    companion object {
        const val EXTRA_REPLY = "NoteTitle"
        const val EXTRA_REPLY_ID = "NoteId"
        const val EXTRA_REPLY_DATE = "NoteDate"

    }

    lateinit var nDate : String
    lateinit var noteDate: TextView
    lateinit var textAlarm: TextView
//    lateinit var alarmImage: ImageView
    lateinit var dateImage: ImageView
    val date = arrayListOf(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
    var hasAlarm = false
    lateinit var noteViewModel: NoteViewModel
    var nImages = arrayListOf<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note_details)
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        val dateButton: ImageButton = findViewById(R.id.date_button)
      //  val alarmButton: ImageButton = findViewById(R.id.time_button)
       // alarmImage = findViewById(R.id.image_view_alarm_date)
        dateImage = findViewById(R.id.image_view_detail_date)
        val saveButton: FloatingActionButton = findViewById(R.id.save_button)
        val noteTitle: EditText = findViewById(R.id.detail_title)
        val noteDetail: EditText = findViewById(R.id.detail_description)
        noteDate = findViewById(R.id.text_view_detail_date)
        textAlarm = findViewById(R.id.text_view_alarm_date)
        noteViewModel = NoteViewModelFactory(application).create(NoteViewModel::class.java)
        val intent = intent


        var isEmpty = false
        var noteId = 0
        var noteLiveData: LiveData<Note>?


        //Insert note
        intent?.let {
            noteId = intent.getIntExtra(EXTRA_REPLY_ID, 0)
            if (noteId > 0) {
                noteLiveData = noteViewModel.getNote(noteId)
                noteLiveData!!.observe(this) {
                    isEmpty = false

                    noteTitle.setText(it.title)
                    noteDetail.setText(it.taskCreator)
                    if (it.year != 0) {
                        noteDate.text = "${it.year}/${it.month}/${it.day} Time ${it.nHours} : ${it.min}"

                    }

                    date[0] = it.year
                    date[1] = it.month
                    date[2] = it.day
                    date[3] = it.nHours
                    date[4] = it.min
                    nDate = (( Date(it.year, it.month, it.day, it.nHours, it.min).time ).toString())

                }
            }
        }

        //Save or update the note
        saveButton.setOnClickListener {
            val replyIntent = Intent()
            if (TextUtils.isEmpty(noteTitle.text) || TextUtils.isEmpty(noteDate.text.trim())) {
                isEmpty = true;
                val show: Any = AlertDialog.Builder(this)
                    .setTitle("You Have To Insert Title and Date")
                    .setMessage("Exit without Save")
                    .setPositiveButton(
                        "No"
                    ) { dialog, _ ->
                        dialog.dismiss()
                    }.setNegativeButton(
                        "Exit"
                    ) { dialog, _ ->
                        setResult(Activity.RESULT_CANCELED, replyIntent)
                        finish()
                        dialog.dismiss()
                    }.show()
            } else {

                  val duplicateCount =  noteViewModel.checkDuplicate(
                      noteTitle.text.toString(),
                      (nDate)
                  );
                    duplicateCount!!.observe(this){
                        if(it.toInt() == 0 || ( noteId > 0 && it.toInt() <= 1 ) ) {
                            noteViewModel.update(
                                Note(
                                    noteId,
                                    noteTitle.text.toString(),
                                    noteDetail.text.toString(),
                                    date, nDate
                                )
                            )
                            val array: Array<String> = arrayOf(
                                noteTitle.text.toString(),
                                noteDetail.text.toString()
                            )
                            replyIntent.putExtra(EXTRA_REPLY, array)
                            replyIntent.putExtra(EXTRA_REPLY_DATE, date)
                            setResult(Activity.RESULT_OK, replyIntent)
                            finish()
                        }else{
                            val show: Any = AlertDialog.Builder(this)
                                .setTitle("You Have Created Duplicate Note with same Title and Date")
                                .setMessage("Exit without Save")
                                .setPositiveButton(
                                    "No"
                                ) { dialog, _ ->
                                    dialog.dismiss()
                                }.setNegativeButton(
                                    "Exit"
                                ) { dialog, _ ->
                                    setResult(Activity.RESULT_CANCELED, replyIntent)
                                    finish()
                                    dialog.dismiss()
                                }.show()
                        }
                    }


            }

        }


        //Insert date
        dateButton.setOnClickListener {
            pickDateTime(true)
        }


    }

    override fun onBackPressed() {
        val show: Any = AlertDialog.Builder(this)
            .setTitle("Note will not be save")
            .setMessage("Are you sour you want to leave ?")
            .setPositiveButton(
                "YES"
            ) { dialog, _ -> // The user wants to leave - so dismiss the dialog and exit
                finish()
                dialog.dismiss()
            }.setNegativeButton(
                "BACK"
            ) { dialog, _ -> // The user is not sure, so you can exit or just stay
                dialog.dismiss()
            }.show()

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (resultCode == Activity.RESULT_OK && intent != null) {

            nImages.add(intent.data.toString())
            noteViewModel.noteImages.value = nImages
        }
    }


    private fun pickDateTime(isDate: Boolean) {
        val currentDateTime = Calendar.getInstance()
        val startYear = currentDateTime.get(Calendar.YEAR)
        val startMonth = currentDateTime.get(Calendar.MONTH)
        val startDay = currentDateTime.get(Calendar.DAY_OF_MONTH)
        val startHour = currentDateTime.get(Calendar.HOUR_OF_DAY)
        val startMinute = currentDateTime.get(Calendar.MINUTE)

        DatePickerDialog(this, DatePickerDialog.OnDateSetListener { _, year, month, day ->
            TimePickerDialog(this, TimePickerDialog.OnTimeSetListener { _, hour, minute ->
                if (isDate) {
                    date[0] = year
                    date[1] = month + 1
                    date[2] = day
                    date[3] = hour
                    date[4] = minute
                    if (year != 0) {
                        noteDate.text =
                            "${date[0]}/${date[1]}/${date[2]}  Time ${date[3]}:${date[4]}"
                        nDate = (( Date(date[0],date[1], date[2], date[3], date[4]).time ).toString())
                    }
                }
            }, startHour, startMinute, false).show()
        }, startYear, startMonth, startDay).show()
    }



}

