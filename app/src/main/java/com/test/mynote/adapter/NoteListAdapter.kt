package com.test.mynote.adapter

import android.app.AlertDialog
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.test.mynote.NoteDetails
import com.test.mynote.R
import com.test.mynote.database.Note
import com.test.mynote.viewmodel.NoteViewModel
import java.util.*

class NoteListAdapter(private val noteViewModel: NoteViewModel) :
    ListAdapter<Note, NoteListAdapter.ViewHolder>(NOTES_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val cardView = LayoutInflater.from(parent.context)
            .inflate(R.layout.note_card, parent, false)
        return ViewHolder(cardView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val note = getItem(position)
        holder.create(note, noteViewModel)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val title: TextView = view.findViewById(R.id.note_title)
        private val date: TextView = view.findViewById(R.id.date)
        private val detail: TextView = view.findViewById(R.id.card_detail)
        private val deleteButton: ImageButton = view.findViewById(R.id.button)
        private val cardColor: ImageView = view.findViewById(R.id.color)
        private val cardView: CardView = view.findViewById(R.id.note_card)

        fun create(note: Note, noteViewModel: NoteViewModel) {
            title.text = note.title
            detail.text = note.taskCreator
            date.text = if (note.month > 0) {
                if (!noteViewModel.yearList.contains(Date(note.year, note.month, note.day))) {
                    val date = Date(note.year, note.month, note.day)
                    noteViewModel.yearList.add(date)
                    val currentDateTime = Calendar.getInstance()
                    val startYear = currentDateTime.get(Calendar.YEAR)
                    val startMonth = currentDateTime.get(Calendar.MONTH)+1
                    val startDay = currentDateTime.get(Calendar.DAY_OF_MONTH)
                    var day =0
                    if(note.year==startYear) {
                        if (note.month == startMonth) {
                            if (note.day == startDay)
                                day = 1
                            else if(note.day == startDay+1)
                                day =2
                        }
                    }
                    when(day){
                        1 -> "Today"
                        2 -> "Tomorrow"
                        else -> "${note.year}/${note.month}/${note.day}"
                    }
                } else ""
            } else null



            itemView.setOnClickListener {
                val intent = Intent(it.context, NoteDetails::class.java)
                intent.putExtra(NoteDetails.EXTRA_REPLY_ID, note.id)
                it.context.startActivity(intent)
            }

            deleteButton.setOnClickListener { view ->

                //Set Dialog
                val builder: AlertDialog.Builder? = this.let {
                    val builder = AlertDialog.Builder(view.context)
                    builder.apply {
                        setPositiveButton(
                            R.string.ok
                        ) { _, _ ->
                            noteViewModel.deleteNote.value = note.id
                        }
                        setNegativeButton(
                            R.string.cancel
                        ) { _, _ ->
                        }
                    }
                }
                builder?.setMessage(R.string.dialog_message)
                    ?.setTitle(R.string.dialog_title)
                val dialog: AlertDialog? = builder?.create()
                dialog!!.show()
            }
        }
    }

    companion object {
        private val NOTES_COMPARATOR = object : DiffUtil.ItemCallback<Note>() {
            override fun areContentsTheSame(oldItem: Note, newItem: Note): Boolean {
                return oldItem.title == newItem.title
            }

            override fun areItemsTheSame(oldItem: Note, newItem: Note): Boolean {
                return oldItem == newItem
            }
        }
    }


}