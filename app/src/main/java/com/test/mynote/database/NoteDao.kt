package com.test.mynote.database

import androidx.room.*
import kotlinx.coroutines.flow.Flow


@Dao
interface NoteDao {

    @Insert
    suspend fun insert(note: Note)

    @Update
    suspend fun updateNote(note: Note)

    @Query("SELECT *  FROM note_table")
    fun getAllNote(): Flow<List<Note>?>


    @Query("DELETE FROM note_table")
    suspend fun deleteAll()

    @Query("DELETE FROM note_table WHERE id =:key")
    suspend fun delete(key: Int)

    @Query("Select * from note_table WHERE id = :key")
    fun getNote(key: Int): Flow<Note>

    @Query("Select * from note_table WHERE title = :key")
    fun getNoteByTitle(key: String): Note?

//AND dateTime = :dateTime_key
    @Query("Select COUNT(*) from note_table WHERE  ( title = :title_key  AND    dateTime = :dateTime_key ) "  )
    fun  checkDuplicateItem(title_key : String, dateTime_key: String) : Flow<Int>

    @Query("Update note_table SET year = :year,month= :month,day= :day WHERE  id = :key")
    suspend fun updateAlarm(key: Int,year: Int,month:Int,day:Int)

}