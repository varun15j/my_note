package com.test.mynote.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.ArrayList;
import java.util.Date;


import ir.mirrajabi.searchdialog.core.Searchable;

@Entity(tableName = "note_table")
public class Note implements Searchable {
    @PrimaryKey(autoGenerate = true)
    public Integer id;
    @ColumnInfo(name = "title")
    public String title;
    @ColumnInfo(name = "detail")
    public String taskCreator;

    @ColumnInfo(name = "year")
    public Integer year;
    @ColumnInfo(name = "month")
    public Integer month;
    @ColumnInfo(name = "day")
    public Integer day;
    @ColumnInfo(name = "n_hours")
    public Integer nHours = 0;
    @ColumnInfo(name = "min")
    public Integer min = 0;

     @ColumnInfo(name = "dateTime")
    public String dateTime ;

    public Note(String title, String taskCreator) {
        this.title = title;
        this.taskCreator = taskCreator;
    }


    public Note(Integer id, String title, String taskCreator, ArrayList<Integer> date, String timestamp) {
        this.id = id;
        this.title = title;
        this.taskCreator = taskCreator;
        year = date.get(0);
        month = date.get(1);
        day = date.get(2);
        nHours = date.get(3);
        min = date.get(4);

        dateTime = timestamp;
    }

    public Note(  String title, String taskCreator, ArrayList<Integer> date) {

        this.title = title;
        this.taskCreator = taskCreator;
        year = date.get(0);
        month = date.get(1);
        day = date.get(2);
        nHours = date.get(3);
        min = date.get(4);

        dateTime = Long.toString(new Date(year, month, day, nHours,min ).getTime());
    }


    public Note() {
    }

    @Override
    public String getTitle() {
        return title;
    }
}